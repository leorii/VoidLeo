use lazy_static::lazy_static;
use ron;
use serde::Deserialize;
use serenity::model::{
    channel::ReactionType,
    id::{ChannelId, GuildId, RoleId, UserId},
};
use std::{collections::HashSet, env, fs, path::PathBuf, sync::Arc};

lazy_static! {
    static ref CONFIG: Arc<AppConfig> = Arc::new(AppConfig::init());
}

/// The configuration struct for VoidLeo. Your config.ron file will be deserialized into this
/// config struct at program start and made available everywhere in the program. Id's will be
/// coerced from integers.
///
/// # Example
/// ```
/// // config.ron
///
/// AppConfig (
///     discord_token: "asdf",
///     guild_id: 3456789,
///     bot_user_id: 8746936,
///     owners: [23674, 34856],
///     command_permissions: (
///         ping: None,
///     ),
///     log_channel_id: None,
///     new_member_welcome: None,
///     emoji_pings: None,
///     lurker_purge: None,
/// )
/// ```
#[derive(Clone, Default, Deserialize)]
pub struct AppConfig {
    pub discord_token: String,
    pub guild_id: GuildId,
    pub bot_user_id: UserId,
    pub owners: HashSet<UserId>,
    pub command_permissions: CommandPermissions,

    pub log_channel_id: Option<ChannelId>,
    pub new_member_welcome: Option<NewMemberWelcome>,
    pub emoji_pings: Option<Vec<EmojiPingConfig>>,
    pub lurker_purge: Option<LurkerPurgeConfig>,
}

/// You can restrict command execution to a list of roles in this section.
///
/// # Example
///
/// ```
/// // config.ron
///
/// AppConfig (
///     ...,
///     command_permissions: (
///         ping: None,
///     ),
/// )
/// ```
#[derive(Clone, Default, Deserialize)]
pub struct CommandPermissions {
    pub ping: Option<HashSet<RoleId>>,
}

/// You can greet new members with a custom message with this section. If you supply a
/// `ping_insert_idx`, then a mention for the new user will be inserted at that index in the
/// `message`.
///
/// # Example
/// ```
/// // config.ron
///
/// AppConfig (
///     ...,
///     new_member_welcome: Some((
///         message: "@everyone welcome  to the server!",
///         channel_id: 2376583,
///         ping_insert_idx: Some(18), // "@everyone welcome <inserted_here> to the server!"
///     )),
/// )
/// ```
#[derive(Clone, Deserialize)]
pub struct NewMemberWelcome {
    pub message: String,
    pub channel_id: ChannelId,
    pub ping_insert_idx: Option<usize>,
}

/// Emoji pings allow you to assign emojis to users. Whenever that emoji is posted by itself in a
/// channel, VoidLeo will ping the user and immediately delete the ping. The user still gets the
/// ping notification, and it appears as if they were pinged just by the emoji.
///
/// # Example
/// ```
/// // config.ron
///
/// AppConfig (
///     ...,
///     emoji_pings: Some([(
///         user_id: 938579827,
///         emojis: ["<:roopog:938529384572>", "📷"],
///     ), (
///         user_id: 238562783,
///         emojis: ["🐺"],
///     )]),
/// )
/// ```
#[derive(Clone, Deserialize)]
pub struct EmojiPingConfig {
    pub user_id: UserId,
    pub emojis: Vec<String>,
}

/// This command will post a message in the given channel, and react to that message with the given
/// ReactType. Users in this channel will have `grace_period_days` to click the reaction on the
/// message, or they will be kicked from the server after the grace period. Users who have roles
/// included in `immune_roles` will never be kicked.
///
/// # Example
/// ```
/// // config.ron
///
/// AppConfig (
///     ...,
///     lurker_purge: Some((
///         channel_id: 2358612987,
///         grace_period_days: 14,
///         immune_roles: [23875625, 23627234, 93278925],
///         message: "React with <:roocop:2395862395> or be evicted from the server in 14 days!",
///         reaction: {
///             animated: false,
///             id: 2395862395,
///             name: Some("roocop"),
///         },
///     )),
/// )
/// ```
#[derive(Clone, Deserialize)]
pub struct LurkerPurgeConfig {
    pub channel_id: ChannelId,
    pub grace_period_days: u64,
    pub immune_roles: Vec<RoleId>,
    pub message: String,
    pub reaction: ReactionType,
}

impl<'a> AppConfig {
    /// Get an Arc reference to the config.
    pub fn get_arc() -> Arc<Self> {
        CONFIG.clone()
    }

    /// Deserializes the config.ron file and initialized the config.
    fn init() -> Self {
        let config_file: PathBuf = {
            let data_dir = env::var("DATA_DIR").expect("no DATA_DIR env set");

            [&data_dir, "config.ron"].iter().collect()
        };
        let raw_config = fs::read_to_string(config_file).expect("could not read config file");

        ron::from_str(&raw_config).expect("invalid config file format")
    }
}
