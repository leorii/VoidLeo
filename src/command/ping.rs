use crate::{command::CustomCommand, AppConfig};
use serenity::{framework::standard::CommandResult, model::channel::Message, prelude::Context};
use std::sync::Arc;

/// Struct for the `ping` command.
pub struct Ping<'a> {
    ctx: &'a Context,
    msg: &'a Message,
    config: Arc<AppConfig>,
}

impl<'a> CustomCommand<'a> for Ping<'a> {
    /// Creates a new Ping struct.
    fn new(ctx: &'a Context, msg: &'a Message) -> Self {
        Ping {
            ctx,
            msg,
            config: AppConfig::get_arc(),
        }
    }

    /// Runs the `ping` command.
    fn exec(&self) -> CommandResult {
        if !Ping::authorized(self.msg, &self.config.command_permissions.ping) {
            return Ok(());
        }
        self.msg.reply(self.ctx, "Pong!")?;

        Ok(())
    }
}
