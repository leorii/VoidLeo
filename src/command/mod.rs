use serenity::{
    framework::standard::CommandResult,
    model::{channel::Message, id::RoleId},
    prelude::Context,
};
use std::collections::HashSet;

mod lurker_purge;
mod ping;
mod purge_channel_messages;

pub use lurker_purge::LurkerPurge;
pub use ping::Ping;
pub use purge_channel_messages::PurgeChannelMessages;

/// Trait for creating custom commands with VoidLeo
pub trait CustomCommand<'a> {
    /// Creates a new instance of your command struct
    fn new(ctx: &'a Context, msg: &'a Message) -> Self;

    /// Runs the command and returns a `CommandResult`
    fn exec(&self) -> CommandResult;

    /// Checks if the user is authorized to run the command. This should be the first thing called
    /// within your `exec` function, and it should return early if the result is `false`.
    ///
    /// This is the default implementation, and can be overwritten if needed.
    fn authorized(msg: &Message, authorized_roles: &Option<HashSet<RoleId>>) -> bool {
        match (authorized_roles, msg.member.as_ref()) {
            (Some(authorized_roles), Some(member)) => member
                .roles
                .iter()
                .any(|role| authorized_roles.get(role).is_some()),
            (None, _) => true,
            _ => false,
        }
    }
}
