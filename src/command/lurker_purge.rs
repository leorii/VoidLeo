use crate::{
    command::CustomCommand,
    config::AppConfig,
    util::{Embed, Logger},
};
use chrono::Utc;
use lazy_static::lazy_static;
use serenity::{
    framework::standard::CommandResult,
    model::{channel::Message, guild::Member, user::User},
    prelude::Context,
};
use std::{
    error::Error,
    sync::{Arc, Mutex},
    thread,
    time::Duration,
};

const SECONDS_IN_DAY: u64 = 86_400;

lazy_static! {
    static ref THREAD_LOCK: Mutex<()> = Mutex::new(());
}

/// Struct for the `lurker_purge` command.
pub struct LurkerPurge<'a> {
    ctx: &'a Context,
    _msg: &'a Message,
    config: Arc<AppConfig>,
}

impl LurkerPurge<'_> {
    /// LurkerPurge includes a background thread to wait the grace period and then kick inactive
    /// users after the time is up. When the server is started, check to see if we are in the
    /// middle or end of a grace_period, and reinstate the background thread.
    pub fn on_ready(config: Arc<AppConfig>, ctx: Context) {
        let purge_config = match config.lurker_purge.as_ref() {
            Some(x) => x,
            None => return,
        };
        let logger = Logger::new(&ctx);

        // Only attempt to purge users if last message was a purge announcement
        if let Some(Some(message)) = purge_config
            .channel_id
            .messages(&ctx, |retriever| retriever.limit(1))
            .map_err(|e| {
                logger.error(&format!(
                    "Unable to retrieve messages for LurkerPurge on_ready:\n\n{:?}\n\n{}",
                    e,
                    e.source()
                        .map(|x| x.to_string())
                        .unwrap_or_else(|| "No Error Source".to_string()),
                ))
            })
            .ok()
            .map(|x| x.into_iter().last())
        {
            let content = message
                .embeds
                .get(0)
                .map(|x| x.description.clone().unwrap_or_default());
            if content != Some(purge_config.message.clone()) {
                return;
            }

            wait_for_grace_period_and_do_purge(config, ctx, message);
        }
    }
}

impl<'a> CustomCommand<'a> for LurkerPurge<'a> {
    /// Creates a new LurkerPurge command.
    fn new(ctx: &'a Context, msg: &'a Message) -> Self {
        LurkerPurge {
            config: AppConfig::get_arc(),
            ctx,
            _msg: msg,
        }
    }

    /// Runs the `lurker_purge` command.
    fn exec(&self) -> CommandResult {
        let logger = Logger::new(self.ctx);
        let purge_config = match self.config.lurker_purge.as_ref() {
            Some(x) => x,
            None => return Ok(()),
        };
        let message = Embed::new()
            .content("@everyone")
            .descr(&purge_config.message)
            .send(self.ctx, &purge_config.channel_id)
            .map_err(|e| {
                logger.error(&format!("Could not send lurker_purge message: {}", &e));
                e
            })?;

        message
            .react(self.ctx, purge_config.reaction.clone())
            .map_err(|e| {
                logger.warn(&format!(
                    "Could not add reaction to lurker_purge message: {}",
                    &e
                ));
                e
            })?;

        wait_for_grace_period_and_do_purge(self.config.clone(), self.ctx.clone(), message);

        Ok(())
    }
}

/// Wait until the grace period is up in a new thread, and after that time, kick inactive memebers
/// and announce the results of the purge.
fn wait_for_grace_period_and_do_purge(config: Arc<AppConfig>, ctx: Context, message: Message) {
    let logger = Logger::new(&ctx);
    let purge_config = match config.lurker_purge.as_ref() {
        Some(x) => x,
        None => return,
    };
    let sleep_duration = {
        let elapsed_grace_period = Utc::now().timestamp() - message.timestamp.timestamp();
        let remaining_grace_period = (purge_config.grace_period_days * SECONDS_IN_DAY)
            .saturating_sub(elapsed_grace_period as u64);

        Duration::from_secs(remaining_grace_period)
    };

    thread::spawn({
        let reaction = purge_config.reaction.clone();

        move || {
            // we should only have one of these threads running at a time
            let _guard = match THREAD_LOCK.try_lock() {
                Ok(x) => x,
                Err(_) => {
                    return;
                }
            };

            thread::sleep(sleep_duration);

            let reaction_users = config
                .lurker_purge
                .as_ref()
                .unwrap()
                .channel_id
                .reaction_users(&ctx, message.id, reaction, None, None)
                .unwrap();

            let inactive_members = kick_inactive_members(config.clone(), &ctx, &reaction_users);
            announce_results_of_purge(config.clone(), &ctx, &reaction_users, &inactive_members);
        }
    })
    .join()
    .map_err(|e| logger.error(&format!("lurker_purge thread panic! {:?}", e)))
    .ok();
}

/// Kick all members who are not immune and have not reacted to the `lurker_purge` message.
fn kick_inactive_members(
    config: Arc<AppConfig>,
    ctx: &Context,
    reaction_users: &[User],
) -> Vec<Member> {
    let logger = Logger::new(ctx);
    let inactive = inactive_members(config, ctx, reaction_users);

    for member in inactive.iter() {
        let user = member.user.read();
        match member.kick_with_reason(ctx, "Kicked for inactivity") {
            Ok(_) => logger.info(&format!(
                "Kicked **{}** for inactivity as a result of the purge",
                user.name
            )),
            Err(e) => logger.error(&format!(
                "Unable to kick <@{}> during purge: {}",
                user.id, e
            )),
        }
    }
    inactive
}

/// Get all non-immune members who have not reacted to the `lurker_purge` message.
fn inactive_members(config: Arc<AppConfig>, ctx: &Context, reaction_users: &[User]) -> Vec<Member> {
    let purge_config = config.lurker_purge.as_ref().unwrap();
    config
        .guild_id
        .members_iter(&ctx)
        .map(|m| m.unwrap())
        .filter(|m| !reaction_users.iter().any(|r| r.id == m.user.read().id))
        .filter(|m| {
            !purge_config
                .immune_roles
                .iter()
                .any(|immune_role| m.roles.iter().any(|member_role| member_role == immune_role))
        })
        .collect()
}

/// Posts a message with the results of the purge. ie, who was kicked and who remains.
fn announce_results_of_purge(
    config: Arc<AppConfig>,
    ctx: &Context,
    reaction_users: &[User],
    inactive_members: &[Member],
) {
    let logger = Logger::new(ctx);
    let channel_id = config.lurker_purge.as_ref().unwrap().channel_id;
    let kicked = inactive_members
        .iter()
        .map(|m| format!("  - [x] ~~{}~~", m.user.read().name.clone()))
        .collect::<Vec<_>>()
        .join("\n");
    let survivors = active_members(config, ctx, reaction_users)
        .iter()
        .map(|m| format!("  - [·] **{}**", m.user.read().name.clone()))
        .collect::<Vec<_>>()
        .join("\n");

    let message = format!(
        "\
        **Thank you for your participation in the purge.**\n\
        \n\
        Remember users who have fallen:\n{}\n\
        \n\
        Surviving users of the purge:\n{}\
    ",
        kicked, survivors
    );

    if let Some(err) = Embed::new().descr(&message).send(ctx, &channel_id).err() {
        logger.warn(&format!("Could not announce results of purge: {}", err));
    }
}

/// Get all non-immune members who have reacted to the `lurker_purge` message.
fn active_members(config: Arc<AppConfig>, ctx: &Context, reaction_users: &[User]) -> Vec<Member> {
    let purge_config = config.lurker_purge.as_ref().unwrap();
    config
        .guild_id
        .members_iter(&ctx)
        .map(|m| m.unwrap())
        .filter(|m| reaction_users.iter().any(|r| r.id == m.user.read().id))
        .filter(|m| {
            !purge_config
                .immune_roles
                .iter()
                .any(|immune_role| m.roles.iter().any(|member_role| member_role == immune_role))
        })
        .collect()
}
