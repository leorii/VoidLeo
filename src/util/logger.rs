use crate::{color, util::msg, AppConfig};
use log::{debug, error, info, warn};
use serenity::{model::id::ChannelId, prelude::Context};

/// The Logger enum. For all log methods, will attempt to post a message in the specified
/// `log_channel_id` if set in config. It will also log that level to stdout.
#[derive(Clone)]
pub enum Logger {
    StdLogOnly,
    WithLogChannel { ctx: Context, channel_id: ChannelId },
}

impl Logger {
    /// Create a new Logger instance.
    pub fn new(ctx: &Context) -> Self {
        let config = AppConfig::get_arc();
        if let Some(channel_id) = config.log_channel_id {
            Logger::WithLogChannel {
                ctx: ctx.clone(),
                channel_id,
            }
        } else {
            Logger::StdLogOnly
        }
    }

    /// Logs `error` level to stdout.
    pub fn error(&self, message: &str) {
        if let Logger::WithLogChannel { ctx, channel_id } = self {
            if let Err(e) = msg::Embed::new()
                .title("[ ERROR ]")
                .descr(message)
                .color(color::LUMINOUS_VIVID_PINK)
                .send(ctx, channel_id)
            {
                warn!("Could not send error message to log channel: {}", e);
            }
        }

        error!("{}", message);
    }

    /// Logs `warn` level to stdout.
    pub fn warn(&self, message: &str) {
        if let Logger::WithLogChannel { ctx, channel_id } = self {
            if let Err(e) = msg::Embed::new()
                .title("[ WARN ]")
                .descr(message)
                .color(color::RED)
                .send(ctx, channel_id)
            {
                warn!("Could not send warn message to log channel: {}", e);
            }
        }

        warn!("{}", message);
    }

    /// Logs `info` level to stdout.
    pub fn info(&self, message: &str) {
        if let Logger::WithLogChannel { ctx, channel_id } = self {
            if let Err(e) = msg::Embed::new()
                .title("[ INFO ]")
                .descr(message)
                .send(ctx, channel_id)
            {
                warn!("Could not send info message to log channel: {}", e);
            }
        }

        info!("{}", message);
    }

    /// Logs `debug` level to stdout.
    pub fn debug(&self, message: &str) {
        if let Logger::WithLogChannel { ctx, channel_id } = self {
            if let Err(e) = msg::Embed::new()
                .title("[ DEBUG ]")
                .descr(message)
                .color(color::PURPLE)
                .send(ctx, channel_id)
            {
                warn!("Could not send debug message to log channel: {}", e);
            }
        }

        debug!("{}", message);
    }
}
