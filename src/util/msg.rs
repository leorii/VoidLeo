use crate::color;
use serenity::{
    model::{channel::Message, id::ChannelId},
    prelude::Context,
    utils::Color,
    Result,
};

/// A helper struct for creating messages that included embeds.
#[derive(Clone)]
pub struct Embed {
    content: Option<String>,
    title: Option<String>,
    description: Option<String>,
    color: u32,
}

impl Embed {
    /// Creates a new Embed instance.
    pub fn new() -> Self {
        Embed {
            content: None,
            title: None,
            description: None,
            color: color::GOLD,
        }
    }

    /// Gives the embed message a title.
    pub fn title(mut self, title: &str) -> Self {
        self.title = Some(title.into());
        self
    }

    /// Gives the embed message a description.
    pub fn descr(mut self, descr: &str) -> Self {
        self.description = Some(descr.into());
        self
    }

    /// Gives the embed message a color.
    pub fn color(mut self, color: u32) -> Self {
        self.color = color;
        self
    }

    /// Includes plain text content _above_ the embed. This is useful for mentioning users or
    /// roles, as any mentions inside an embed don't actually ping the users.
    pub fn content(mut self, content: &str) -> Self {
        self.content = Some(content.into());
        self
    }

    /// Sends the built embed message.
    pub fn send(self, ctx: &Context, channel_id: &ChannelId) -> Result<Message> {
        channel_id.send_message(ctx, move |m| {
            if let Some(ref content) = self.content {
                m.content(content);
            }

            m.embed(|e| {
                if let Some(title) = self.title {
                    e.title(title);
                }
                if let Some(description) = self.description {
                    e.description(description);
                }
                e.color(Color::new(self.color));
                e
            });
            m
        })
    }
}

impl Default for Embed {
    fn default() -> Self {
        Self::new()
    }
}
