//! **VoidLeo** is a Discord bot created for Leorii's server.
//!
//! Feel free to look around, use it on your server, and of course, contribute!
//!
//!
//! # Features
//!
//! * **Emoji Pings**: Ping users with emojis.
//! * **Lurker Purging**: Remove inactive members automatically each month by having them react to a
//! message to prove they're still active.
//! * **Events**: Create event messages easily and use reactions to see who will attend.
//! * Fully configurable to your server's needs.
//!
//!
//! # Commands
//!
//! All commands are prefixed with the `$` sign. You can do `$<command>`, or have a space after the
//! prefix like so: `$ <command>`.
//!
//! **Event**
//!
//! Posts an event message in the current channel and adds a reaction emoji for people to click if
//! they can make it. **VoidLeo** will remove his own reaction after 60 seconds so the attendance
//! numbers are accurate.
//!
//! ```
//! Usage:
//!     $ event [<role_mention>] <reaction_emoji> <message>
//! ```
//!
//! **Ping**
//!
//! Mostly used to check if **VoidLeo** is funtioning properly. **VoidLeo** will respond to the user with
//! "Pong!"
//!
//! ```
//! Usage:
//!     $ ping
//! ```
//!
//! **Lurker Purge**
//!
//! The behaviour of this command is adjusted in the config. Posts a message in the configured
//! channel, and adds a reaction to the message. **VoidLeo** will wait for a number of days and then
//! kick all members in the channel who have not reacted and who do not have immunity from roles.
//! The kick message will be "Kicked for inactivity".
//!
//! _Note_: This command is only available to owners.
//!
//! ```
//! Usage:
//!     $ lurker_purge
//! ```
//!
//! **Purge Channel Messages**
//!
//! Deletes **all** messages in the current channel. Using the command with no options will present
//! the user with a warning message.
//!
//! _Note_: This command is only available to owners.
//!
//! ```
//! Usage:
//!     $ purge_channel_messages [options]
//!
//! Options:
//!     --confirm=<channel_id>  The id of the channel to purge
//!     --after=<message_id>    Only deletes messages that were posted after this message
//! ```
//!
//!
//! # Configuration
//!
//! **VoidLeo** uses a `config.ron` file to store his configuration data. Detailed info about the
//! config file can be found in the `config` module.

pub mod color;
pub mod command;
pub mod config;
pub mod util;

mod handler;

pub use config::AppConfig;
pub use handler::Handler;
