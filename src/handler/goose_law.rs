use serenity::{model::channel::Message, prelude::Context};
use std::{thread::sleep, time::Duration};

pub fn handle(ctx: &Context, msg: &Message) {
    let convo = [
        "I dunno, I think geese are lovely creatures.",
        "I'd own a goose if I could, but.. you know",
        "There's that goose law, only one goose per household. \
            And my Mamma's already got one.",
        "I can't have my own, so",
        "You know maybe if the law changes I can get my own goose, \
            but until then I'll just have to take care of Mamma's goose.",
    ];

    sleep(Duration::from_millis(1200));

    msg.channel_id.say(&ctx, convo[0]).ok();
    sleep(Duration::from_millis(1700));

    msg.channel_id.say(&ctx, convo[1]).ok();
    sleep(Duration::from_millis(2800));

    msg.channel_id.say(&ctx, convo[2]).ok();
    sleep(Duration::from_millis(3300));

    msg.channel_id.say(&ctx, convo[3]).ok();
    sleep(Duration::from_millis(2000));

    msg.channel_id.say(&ctx, convo[4]).ok();
}
