use crate::{config::EmojiPingConfig, util::Logger};
use serenity::{model::channel::Message, prelude::Context};

pub fn handle(logger: &Logger, ctx: &Context, msg: &Message, emoji_pings: &[EmojiPingConfig]) {
    for user_id in emoji_pings
        .iter()
        .filter(|ep| ep.emojis.iter().any(|e| e == &msg.content))
        .map(|ep| &ep.user_id)
    {
        if let Ok(ping) = msg
            .channel_id
            .say(&ctx, format!("<@{}>", user_id))
            .map_err(|e| {
                logger.error(&format!(
                    "Unable to send emoji ping for <@{}>: {}",
                    user_id, e
                ))
            })
        {
            ping.delete(ctx)
                .map_err(|e| {
                    logger.warn(&format!(
                        "Unable to delete emoji ping message for <@{}>: {}",
                        user_id, e,
                    ))
                })
                .ok();
        }
    }
}
