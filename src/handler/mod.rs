mod emoji_pings;
mod goose_law;

use crate::{command, util::Logger, AppConfig};
use serenity::{
    model::{channel::Message, gateway::Ready, guild::Member, id::GuildId},
    prelude::{Context, EventHandler},
};
use std::sync::Arc;

/// This is the Serenity handler for VoidLeo. It contains a reference to the config for easy
/// access.
pub struct Handler {
    config: Arc<AppConfig>,
}

impl Handler {
    /// Creates a new handler.
    pub fn new(config: Arc<AppConfig>) -> Self {
        Handler { config }
    }
}

impl EventHandler for Handler {
    /// This will be called when the Serenity server is started and ready.
    fn ready(&self, ctx: Context, _data_about_bot: Ready) {
        command::LurkerPurge::on_ready(self.config.clone(), ctx);
    }

    /// This will be called for every message sent to the server.
    fn message(&self, ctx: Context, msg: Message) {
        let logger = Logger::new(&ctx);

        if msg.content.to_lowercase()
            == format!("hey <@!{}>, what do you think?", self.config.bot_user_id)
        {
            goose_law::handle(&ctx, &msg);
        }

        // Handles emoji pings if enabled in config
        if let Some(ref emoji_pings) = self.config.emoji_pings {
            emoji_pings::handle(&logger, &ctx, &msg, emoji_pings);
        }
    }

    /// This will be called any time a new member is added to the Guild.
    fn guild_member_addition(&self, ctx: Context, _guild_id: GuildId, new_member: Member) {
        let logger = Logger::new(&ctx);

        // Handles new member welcome messages if enabled in config
        if let Some(welcome_config) = &self.config.new_member_welcome {
            let user_ping = format!("<@{}>", new_member.user.read().id);
            let message = if let Some(i) = welcome_config.ping_insert_idx {
                let mut m = welcome_config.message.clone();
                m.insert_str(i, &user_ping);
                m
            } else {
                welcome_config.message.clone()
            };

            welcome_config
                .channel_id
                .say(ctx, message)
                .map_err(|e| {
                    logger.error(&format!(
                        "Unable to send welcome message to {}: {}",
                        user_ping, e
                    ))
                })
                .ok();
        }
    }
}
